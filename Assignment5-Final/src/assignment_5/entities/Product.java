/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_5.entities;

/**
 *
 * @author kasai
 */
public class Product {
    int productId;
    int minPrice;
    int maxPrice;
    int targetPrice;
    double averagePrice;
    double diff_between_price;
    double error;

    public double getError() {
        return error;
    }

    public void setError(double error) {
        this.error = error;
    }

    public double getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(double averagePrice) {
        this.averagePrice = averagePrice;
    }

    public double getDiff_between_price() {
        return diff_between_price;
    }

    public void setDiff_between_price(double diff_between_price) {
        this.diff_between_price = diff_between_price;
    }

    public Product(int productId, int minPrice, int maxPrice, int targetPrice){
        this.productId = productId;
        this.maxPrice = maxPrice;
        this.minPrice = minPrice;
        this.targetPrice = targetPrice;
    }
    
    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }

    public int getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(int maxPrice) {
        this.maxPrice = maxPrice;
    }

    public int getTargetPrice() {
        return targetPrice;
    }

    public void setTargetPrice(int targetPrice) {
        this.targetPrice = targetPrice;
    }
    
    @Override
    public String toString() {
        //return "Product{" + "productId = " + productId + ", maxPrice = " + maxPrice + ", minPrice = " + minPrice + ", targetPrice = " + targetPrice + '}';
        return "Product{" + "productId = " + productId + ", averagePrice = " + averagePrice + ", targetPrice = " + targetPrice + ", difference between two prices= " + diff_between_price + '}';
    }
}
