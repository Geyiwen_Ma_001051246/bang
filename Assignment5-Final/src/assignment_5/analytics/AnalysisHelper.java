/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_5.analytics;

import assignment_5.entities.Customer;
import assignment_5.entities.Item;
import assignment_5.entities.Order;
import assignment_5.entities.Product;
import assignment_5.entities.SalesPerson;

import java.util.*;

/**
 * @author kasai
 */
public class AnalysisHelper {

    //Top 3 best negotiated products
    public void ThreeMostPopularProduct() {
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        Map<Integer, Item> items = DataStore.getInstance().getItems();
        Map<Integer, Integer> productSoldCountMap = new HashMap<Integer, Integer>();
        for (Item item : items.values()) {
            int productId = item.getProductId();
            Product product = products.get(productId);
            if (item.getSalesPrice() > product.getTargetPrice()) {                
                int count = productSoldCountMap.getOrDefault(productId, 0);
                productSoldCountMap.put(productId, count + item.getQuantity());
            }
        }      
        
        List<Map.Entry<Integer, Integer>> productSoldCountRankMap = new ArrayList<Map.Entry<Integer, Integer>>(productSoldCountMap.entrySet());
        Collections.sort(productSoldCountRankMap, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return o2.getValue() - o1.getValue();
            }
        });

        System.out.println("\n3 Most Popular Products: ");
        int forcount=1;
        for (int i = 0; i < productSoldCountRankMap.size(); ) {
            System.out.println("Top"+forcount+":");
            System.out.println("Product id: " + productSoldCountRankMap.get(i).getKey() + " count: " + productSoldCountRankMap.get(i).getValue());
            for(int j=i+1;j<productSoldCountRankMap.size();j++){
            if(productSoldCountRankMap.get(j).getValue().equals(productSoldCountRankMap.get(i).getValue())){
            System.out.println("Product id: " + productSoldCountRankMap.get(j).getKey() + " count: " + productSoldCountRankMap.get(j).getValue());
            //j=j+1;
            }
            else{
                forcount=forcount+1;
                i=j;
                //System.out.println("\n");
                break;
            }
            }
           if(forcount>3){
               break;
           }
        }

    }

    //Top 3 best customers
    public void ThreeBestCustomers() {
        Map<Integer, Customer> customers = DataStore.getInstance().getCustomers();
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        Map<Integer, Integer> customerPriceDetaMap= new HashMap<>();
        for (Customer customer : customers.values()) {
            int detaSum = 0;
            for (Order order : customer.getOrders()) {
                Product product = products.get(order.getItem().getProductId());
                // 销售价格(sale price)与目标价格(target price)之间的差
                int deta = Math.abs(order.getItem().getSalesPrice() - product.getTargetPrice());
                detaSum += deta * order.getItem().getQuantity();
            }
            customerPriceDetaMap.put(customer.getCustomerId(), Math.abs(detaSum));
        }

        List<Map.Entry<Integer, Integer>> customerPriceRankMap = new ArrayList<Map.Entry<Integer, Integer>>(customerPriceDetaMap.entrySet());
        Collections.sort(customerPriceRankMap, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return o1.getValue() - o2.getValue();
            }
        });

        System.out.println("\n3 Best Customer: ");
        int forcount=1;
        for (int i = 0; i < customerPriceRankMap.size(); ) {
            System.out.println("Top"+forcount+":");
            System.out.println("Customer id: " + customerPriceRankMap.get(i).getKey() + " deta: " + customerPriceRankMap.get(i).getValue());
            for(int j=i+1;j<customerPriceRankMap.size();j++){
            if(customerPriceRankMap.get(j).getValue().equals(customerPriceRankMap.get(i).getValue())){
            System.out.println("Customer id: " + customerPriceRankMap.get(j).getKey() + " deta: " + customerPriceRankMap.get(j).getValue());
            //j=j+1;
            }
            else{
                forcount=forcount+1;
                i=j;
                //System.out.println("\n");
                break;
            }
            }
           if(forcount>3){
               break;
           }
        }
    }

    //Top 3 best sales people 
    public void ThreeBestSalesPerson() {
        Map<Integer, SalesPerson> sales = DataStore.getInstance().getSales();
        Map<Integer, Integer> customerMap = new HashMap<>();
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        for (SalesPerson salesPerson : sales.values()) {
            int totalBonus = 0;
            for (Order order : salesPerson.getOrders()) {
                Product product = products.get(order.getItem().getProductId());
                // 单个商品利润：销售价格-目标价格
                int bonus = order.getItem().getSalesPrice() - product.getTargetPrice();
                totalBonus += bonus * order.getItem().getQuantity();
            }
            customerMap.put(salesPerson.getSalesId(), totalBonus);
        }

        List<Map.Entry<Integer, Integer>> customerBonusRankMap = new ArrayList<Map.Entry<Integer, Integer>>(customerMap.entrySet());
        Collections.sort(customerBonusRankMap, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return o2.getValue() - o1.getValue();
            }
        });
        System.out.println("\n3 Best SalesPerson: ");
        int forcount=1;
        for (int i = 0; i < customerBonusRankMap.size(); ) {
            System.out.println("Top"+forcount+":");
            System.out.println("SalesPerson id: " + customerBonusRankMap.get(i).getKey() + " Total Bonus: " + customerBonusRankMap.get(i).getValue());
            for(int j=i+1;j<customerBonusRankMap.size();j++){
            if(customerBonusRankMap.get(j).getValue().equals(customerBonusRankMap.get(i).getValue())){
            System.out.println("SalesPerson id: " + customerBonusRankMap.get(j).getKey() + " Total Bonus: " + customerBonusRankMap.get(j).getValue());
            //j=j+1;
            }
            else{
                forcount=forcount+1;
                i=j;
                //System.out.println("\n");
                break;
            }
            }
           if(forcount>3){
               break;
           }
        }
    }

    //Total revenue for the year 
    public void TotalRevenueForTheYear() {
        Map<Integer, Item> items = DataStore.getInstance().getItems();
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        int Total = 0;
        for (Item i : items.values()) {
            int bonus = 0;
            for (Product p : products.values()) {
                if (p.getProductId() == i.getProductId()) {
                    bonus = i.getSalesPrice() - p.getTargetPrice();
                }
            }
            Total += bonus * i.getQuantity();
        }
        System.out.println("\nThe total revenue for the year:" + Total+"\n");
    }
    
    //For original data
    public void getOriginalData(){
        Map<Integer, Item> items=DataStore.getInstance().getItems();
        Map<Integer, Product> products=DataStore.getDataStore().getProducts();
        List<Product> prodList=new ArrayList<Product>(products.values());
        
        for(Product p:products.values()){
            double averagePrice=0;
            int quantity=0;
            int totalPrice=0;
            for(Item i:items.values()){
                if(i.getProductId()==p.getProductId()){
                    quantity=quantity+i.getQuantity();
                    totalPrice=totalPrice+i.getSalesPrice()*i.getQuantity();
                }
            }
            averagePrice=totalPrice/quantity;
            p.setAveragePrice(averagePrice);
            p.setDiff_between_price(averagePrice-p.getTargetPrice());
        }
        
        Collections.sort(prodList,new Comparator<Product>(){
            @Override 
            public int compare(Product p1,Product p2){
                return (int)p2.getDiff_between_price()-(int)p1.getDiff_between_price();
            }});
        
        System.out.println("Original Data Model:");
        System.out.print("Products(Average Price >= Target Price):\n");
        for(int i=0;i<prodList.size();i++){
            if(prodList.get(i).getDiff_between_price()>=0){
            System.out.println(prodList.get(i));}
        }       
        //System.out.println("\n");
        
        System.out.print("Products(Average Price < Target Price):\n");
        for(int i=0;i<prodList.size();i++){
            if(prodList.get(i).getDiff_between_price()<0){
            System.out.println(prodList.get(i));}
        }       
        System.out.println("\n");
    }
    
    //For modified data
    public void getModifiedData(){
        //Map<Integer, Item> items=DataStore.getInstance().getItems();
        Map<Integer, Product> products=DataStore.getDataStore().getProducts();
        List<Product> prodList=new ArrayList<Product>(products.values());
        
        for(Product p:products.values()){
            double averagePrice= p.getAveragePrice();
            if(p.getTargetPrice()<averagePrice-0.05*averagePrice)
            {
                p.setTargetPrice((int)(averagePrice-0.05*averagePrice));
            }
            else if(p.getTargetPrice()>averagePrice+0.05*averagePrice){
                p.setTargetPrice((int)(averagePrice+0.05*averagePrice));
            }
            p.setDiff_between_price(averagePrice-p.getTargetPrice());
            p.setError(((p.getTargetPrice()-averagePrice))/averagePrice);
        }
        
        Collections.sort(prodList,new Comparator<Product>(){
            @Override 
            public int compare(Product p1,Product p2){
                return (int)p2.getDiff_between_price()-(int)p1.getDiff_between_price();
            }});
    
        System.out.println("Modified Data Model:");
        System.out.print("Products(Average Price >= Target Price):\n");
        for(int i=0;i<prodList.size();i++){
            if(prodList.get(i).getDiff_between_price()>=0){
            System.out.println(prodList.get(i));
            System.out.println("Error: "+prodList.get(i).getError());}
        }       
        //System.out.println("\n");
        
        System.out.print("Products(Average Price < Target Price):\n");
        for(int i=0;i<prodList.size();i++){
            if(prodList.get(i).getDiff_between_price()<0){
            System.out.println(prodList.get(i));
            System.out.println("Error: "+prodList.get(i).getError());}
        }       
    }
}
