/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfac.AirLinerManager;

import Business.AirLiner;
import Business.Airplane;
import Business.Flight;
import Business.FlightSchedule;
import java.awt.CardLayout;
import java.awt.Component;
import java.util.Calendar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author 10127
 */
public class UpdateFlightJPanel extends javax.swing.JPanel {
    /**
     * Creates new form UpdateFlightJPanel
     */
    private JPanel CardSequenceJPanel;
    private AirLiner airliner;
    private Airplane airplane;
    private Flight flight;
    private FlightSchedule flightschedule;
    UpdateFlightJPanel(Flight flight, JPanel CardSequenceJPanel, AirLiner airliner, FlightSchedule flightschedule) {
        initComponents();
        this.CardSequenceJPanel=CardSequenceJPanel;
        this.flight=flight;
        this.airliner=airliner;
        this.flightschedule=flightschedule;
        populate();
    }
    
    public void populate(){
        txtFlightNumber.setText(flight.getFlightnumber());
        txtDepatureLocation.setText(flight.getDepartureLocation());
        txtArriveLocation.setText(flight.getArriveLocation());
        txtMonth.setText(flight.getDepartureTime().get(Calendar.MONTH)+"");
        txtDay.setText(flight.getDepartureTime().get(Calendar.DATE)+"");
        txtYear.setText(flight.getDepartureTime().get(Calendar.YEAR)+"");
        txtHour.setText(flight.getDepartureTime().get(Calendar.HOUR)+"");
        txtMinute.setText(flight.getDepartureTime().get(Calendar.MINUTE)+"");
        //Arrive
        txtMonth1.setText(flight.getArriveTime().get(Calendar.MONTH)+"");
        txtDay1.setText(flight.getArriveTime().get(Calendar.DATE)+"");
        txtYear1.setText(flight.getArriveTime().get(Calendar.YEAR)+"");
        txtHour1.setText(flight.getArriveTime().get(Calendar.HOUR)+"");
        txtMinute1.setText(flight.getArriveTime().get(Calendar.MINUTE)+"");
        
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnUpdateFlight = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        txtYear = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtMonth = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtDay = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtHour = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtMinute = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtHour1 = new javax.swing.JTextField();
        txtArriveLocation = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        txtFlightNumber = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtDepatureLocation = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtYear1 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtDay1 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtMonth1 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtMinute1 = new javax.swing.JTextField();

        btnUpdateFlight.setBackground(new java.awt.Color(0, 0, 102));
        btnUpdateFlight.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        btnUpdateFlight.setForeground(new java.awt.Color(255, 255, 255));
        btnUpdateFlight.setText("Update Flight");
        btnUpdateFlight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateFlightActionPerformed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Times New Roman", 0, 32)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(0, 0, 102));
        jLabel14.setText("Update Flight");

        btnSave.setBackground(new java.awt.Color(0, 0, 102));
        btnSave.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        btnSave.setForeground(new java.awt.Color(255, 255, 255));
        btnSave.setText("Save");
        btnSave.setEnabled(false);
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnBack.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        btnBack.setForeground(new java.awt.Color(0, 0, 102));
        btnBack.setText("<<Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        txtYear.setEnabled(false);

        jLabel3.setText("/");
        jLabel3.setEnabled(false);

        txtMonth.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        txtMonth.setEnabled(false);
        txtMonth.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMonthActionPerformed(evt);
            }
        });

        jLabel4.setText("/");
        jLabel4.setEnabled(false);

        txtDay.setEnabled(false);
        txtDay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDayActionPerformed(evt);
            }
        });

        jLabel5.setText("exp:mm/dd/yyyy hh:mm");

        txtHour.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        txtHour.setEnabled(false);
        txtHour.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHourActionPerformed(evt);
            }
        });

        jLabel6.setText(":");
        jLabel6.setEnabled(false);

        txtMinute.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        txtMinute.setEnabled(false);
        txtMinute.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMinuteActionPerformed(evt);
            }
        });

        jLabel7.setText("Depature Location:");

        jLabel13.setText("exp:mm/dd/yyyy hh:mm");
        jLabel13.setEnabled(false);

        txtHour1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        txtHour1.setEnabled(false);
        txtHour1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHour1ActionPerformed(evt);
            }
        });

        txtArriveLocation.setEnabled(false);

        jLabel1.setText("Flight Number:");

        txtFlightNumber.setEnabled(false);

        jLabel2.setText("Depature Time:");

        txtDepatureLocation.setEnabled(false);

        jLabel8.setText("Arrive Location:");

        jLabel9.setText("Arrive Time:");

        txtYear1.setEnabled(false);

        jLabel10.setText("/");
        jLabel10.setEnabled(false);

        txtDay1.setEnabled(false);
        txtDay1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDay1ActionPerformed(evt);
            }
        });

        jLabel11.setText("/");
        jLabel11.setEnabled(false);

        txtMonth1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        txtMonth1.setEnabled(false);
        txtMonth1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMonth1ActionPerformed(evt);
            }
        });

        jLabel12.setText(":");
        jLabel12.setEnabled(false);

        txtMinute1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        txtMinute1.setEnabled(false);
        txtMinute1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMinute1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(262, 262, 262)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel9)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(35, 35, 35)
                                .addComponent(txtArriveLocation, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))))
                .addContainerGap(259, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addGap(351, 351, 351))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(37, 37, 37)
                        .addComponent(btnUpdateFlight)
                        .addGap(306, 306, 306))))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(390, 390, 390)
                            .addComponent(txtDepatureLocation, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(390, 390, 390)
                            .addComponent(txtFlightNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(379, 379, 379)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel5)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(txtMonth, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel3)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtDay, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel4)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtYear, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(txtHour, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel6)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtMinute, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addGap(265, 265, 265)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(txtMonth1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel11)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtDay1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel10)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtYear1, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(txtHour1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel12)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtMinute1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jLabel13))))
                    .addContainerGap(265, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jLabel14)
                .addGap(37, 37, 37)
                .addComponent(jLabel1)
                .addGap(63, 63, 63)
                .addComponent(jLabel2)
                .addGap(51, 51, 51)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 62, Short.MAX_VALUE)
                .addComponent(jLabel9)
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtArriveLocation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnUpdateFlight, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(101, 101, 101))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(143, 143, 143)
                    .addComponent(txtFlightNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(21, 21, 21)
                    .addComponent(jLabel5)
                    .addGap(18, 18, 18)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3)
                        .addComponent(txtMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4)
                        .addComponent(txtDay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtHour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6)
                        .addComponent(txtMinute, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(39, 39, 39)
                    .addComponent(txtDepatureLocation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(jLabel13)
                    .addGap(18, 18, 18)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtYear1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel11)
                        .addComponent(txtMonth1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel10)
                        .addComponent(txtDay1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtHour1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel12)
                        .addComponent(txtMinute1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(209, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnUpdateFlightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateFlightActionPerformed
        // TODO add your handling code here:
        txtFlightNumber.setEnabled(true);
        txtDepatureLocation.setEnabled(true);
        txtArriveLocation.setEnabled(true);
        txtMonth.setEnabled(true);
        txtDay.setEnabled(true);
        txtYear.setEnabled(true);
        txtHour.setEnabled(true);
        txtMinute.setEnabled(true);
        //Arrive
        txtMonth1.setEnabled(true);
        txtDay1.setEnabled(true);
        txtYear1.setEnabled(true);
        txtHour1.setEnabled(true);
        txtMinute1.setEnabled(true);
        btnSave.setEnabled(true);
        btnUpdateFlight.setEnabled(false);
    }//GEN-LAST:event_btnUpdateFlightActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
        int month,date,year,hour,minute,month1,date1,year1,hour1,minute1;
        try{
            month = Integer.parseInt(txtMonth.getText());
            date = Integer.parseInt(txtDay.getText());
            year = Integer.parseInt(txtYear.getText());
            hour = Integer.parseInt(txtHour.getText());
            minute = Integer.parseInt(txtMinute.getText());
            month1 = Integer.parseInt(txtMonth1.getText());
            date1 = Integer.parseInt(txtDay1.getText());
            year1 = Integer.parseInt(txtYear1.getText());
            hour1 = Integer.parseInt(txtHour1.getText());
            minute1 = Integer.parseInt(txtMinute1.getText());
            //add some validation here,time and location should not be equal.
            if(txtFlightNumber.getText().equals(flight.getFlightnumber())||String.valueOf(flightschedule.checkFlight(txtFlightNumber.getText())).equals("true")){
            if(month==month1&&date==date1&&year==year1&&hour==hour1&&minute==minute1)
            {
                JOptionPane.showMessageDialog(null, "The DepartureTime you entered is equal to the ArriveTime, please check and enter again!", "Information", JOptionPane.INFORMATION_MESSAGE);
            }
            else{
                if(txtDepatureLocation.getText().equals(txtArriveLocation.getText()))
                {JOptionPane.showMessageDialog(null, "The DepartureLocation you entered is equal to the ArriveLocation, please check and enter again!", "Information", JOptionPane.INFORMATION_MESSAGE);}
                else{
                    flight.setFlightnumber(txtFlightNumber.getText());
                    flight.getDepartureTime().set(year, month - 1, date, hour, minute, 0);
                    flight.getArriveTime().set(year1, month1 - 1, date1, hour1, minute1, 0);
                    flight.setDepartureLocation(txtDepatureLocation.getText());
                    flight.setArriveLocation(txtArriveLocation.getText());
                    JOptionPane.showMessageDialog(null, "Flight Update Successfully!", "Information", JOptionPane.INFORMATION_MESSAGE);
                    
                    txtMonth.setEnabled(false);
                    txtMonth1.setEnabled(false);
                    txtDay.setEnabled(false);
                    txtDay1.setEnabled(false);
                    txtYear.setEnabled(false);
                    txtYear1.setEnabled(false);
                    txtHour.setEnabled(false);
                    txtHour1.setEnabled(false);
                    txtMinute.setEnabled(false);
                    txtMinute1.setEnabled(false);
                    txtFlightNumber.setEnabled(false);
                    txtDepatureLocation.setEnabled(false);
                    txtArriveLocation.setEnabled(false);
                    btnSave.setEnabled(false);
                    btnUpdateFlight.setEnabled(true);
                }
                }
            }
            else{
                JOptionPane.showMessageDialog(null, "The Flight Number you entered exist, please check and enter again!", "Information", JOptionPane.INFORMATION_MESSAGE);
                txtFlightNumber.setText("");
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Please Type In Number In Depature Time And Arrive Time!");
            return;
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        CardSequenceJPanel.remove(this);
        Component[] componentArray = CardSequenceJPanel.getComponents();
        Component component = componentArray[componentArray.length - 1];
        ViewFlightJPanel viewflightjpanel = (ViewFlightJPanel) component;
        viewflightjpanel.refreshtable();
        CardLayout layout=(CardLayout) CardSequenceJPanel.getLayout();
        layout.previous(CardSequenceJPanel);
    }//GEN-LAST:event_btnBackActionPerformed

    private void txtMonthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMonthActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMonthActionPerformed

    private void txtDayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDayActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDayActionPerformed

    private void txtHourActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHourActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtHourActionPerformed

    private void txtMinuteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMinuteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMinuteActionPerformed

    private void txtHour1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHour1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtHour1ActionPerformed

    private void txtDay1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDay1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDay1ActionPerformed

    private void txtMonth1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMonth1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMonth1ActionPerformed

    private void txtMinute1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMinute1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMinute1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdateFlight;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField txtArriveLocation;
    private javax.swing.JTextField txtDay;
    private javax.swing.JTextField txtDay1;
    private javax.swing.JTextField txtDepatureLocation;
    private javax.swing.JTextField txtFlightNumber;
    private javax.swing.JTextField txtHour;
    private javax.swing.JTextField txtHour1;
    private javax.swing.JTextField txtMinute;
    private javax.swing.JTextField txtMinute1;
    private javax.swing.JTextField txtMonth;
    private javax.swing.JTextField txtMonth1;
    private javax.swing.JTextField txtYear;
    private javax.swing.JTextField txtYear1;
    // End of variables declaration//GEN-END:variables
}
