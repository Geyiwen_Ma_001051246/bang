/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author mage
 */
public class AirplaneDirectory {

    private ArrayList<Airplane> airplanelist;

    public AirplaneDirectory() {
        this.airplanelist = new ArrayList<Airplane>();
    }

    public Airplane addAirplane() {
        Airplane airplane = new Airplane();
        airplanelist.add(airplane);
        return airplane;
    }

    public void addAirplane(Airplane airplane) {
        airplanelist.add(airplane);
    }

    public void removeAirplane(Airplane airplane) {
        airplanelist.remove(airplane);

    }

    public ArrayList<Airplane> getAirplanelist() {
        return airplanelist;
    }

    public void setAirplanelist(ArrayList<Airplane> airplanelist) {
        this.airplanelist = airplanelist;
    }

    public ArrayList<Airplane> searchAirLinerbyAFM(String afm) {
        ArrayList<Airplane> a = new ArrayList<Airplane>();
        for (Airplane airplane : airplanelist) {
            if (airplane.getProductModel().equals(afm)) {
                a.add(airplane);
            }
        }

        return a;
    }

    public ArrayList<Airplane> searchAirLinerbySize(String size) {
        ArrayList<Airplane> a = new ArrayList<Airplane>();
        for (Airplane airplane : airplanelist) {
            if (airplane.getSize().equals(size)) {
                a.add(airplane);
            }
        }

        return a;
    }

    public ArrayList<Airplane> searchAirLinerbyUseage(double u) {
        ArrayList<Airplane> a = new ArrayList<Airplane>();
        for (Airplane airplane : airplanelist) {
            if (airplane.getUsedAge() == u) {
                a.add(airplane);
            }
        }

        return a;
    }
    
    public boolean checkModel(String model){
        for(Airplane airplane:airplanelist)
        {
            if(airplane.getProductModel().equals(model)){
                return false;
            }
        }
        return true;
    }

}
