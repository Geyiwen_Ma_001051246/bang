/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author wangbeibei
 */
public class TravelAgency {

    private ArrayList<AirLiner> airlinerlist;
    private CustomerList customerlist;

    public TravelAgency() {
        this.airlinerlist = new ArrayList<AirLiner>();
        this.customerlist = new CustomerList();
    }

    public CustomerList getCustomerlist() {
        return customerlist;
    }

    public AirLiner addAirliner() {
        AirLiner airliner = new AirLiner();
        airlinerlist.add(airliner);

        return airliner;
    }

    public void removeAirLiner(AirLiner airliner) {
        airlinerlist.remove(airliner);

    }

    public ArrayList<AirLiner> getAirlinerlist() {
        return airlinerlist;
    }

    public void setAirlinerlist(ArrayList<AirLiner> airlinerlist) {
        this.airlinerlist = airlinerlist;
    }

    public AirLiner searchAirLinerbyNmae(String Name) {
        for (AirLiner airliner : airlinerlist) {
            if (airliner.getName().equals(Name)) {
                return airliner;
            }
        }
        return null;
    }

    public AirLiner searchAirLinerbyAb(String ab) {
        for (AirLiner airliner : airlinerlist) {
            if (airliner.getAbbreviate().equals(ab)) {
                return airliner;
            }
        }
        return null;
    }
    
    public boolean checkName(String name){
        for(AirLiner airliner:airlinerlist)
        {
            String airName=airliner.getName();
            if(airName.equals(name))
            {
                return false;
            }
        }
        return true;
    }
    
    public boolean checkAbbreviate(String abbreviate){
        for(AirLiner airliner:airlinerlist)
        {
            String airAbbre=airliner.getAbbreviate();
            if(airAbbre.equals(abbreviate))
            {
                return false;
            }
        }
        return true;
    }
}
