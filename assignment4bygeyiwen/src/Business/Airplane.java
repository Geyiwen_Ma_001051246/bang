/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author mage
 */
public class Airplane {
    private String ProductModel;
    private String Size;
    private int Row;
    private int SeatEachRow;
    private int TotalSeatNumber;
    private double UsedAge;
    private FlightSchedule flightschedul;

    public Airplane() {
        this.flightschedul=new FlightSchedule();
    }

    public int getRow() {
        return Row;
    }

    public void setRow(int Row) {
        this.Row = Row;
    }

    public int getSeatEachRow() {
        return SeatEachRow;
    }

    public void setSeatEachRow(int SeatEachRow) {
        this.SeatEachRow = SeatEachRow;
    }

    
    public FlightSchedule getFlightschedul() {
        return flightschedul;
    }

    public void setFlightschedul(FlightSchedule flightschedul) {
        this.flightschedul = flightschedul;
    }
    

    public String getProductModel() {
        return ProductModel;
    }

    public void setProductModel(String ProductModel) {
        this.ProductModel = ProductModel;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String Size) {
        this.Size = Size;
    }

    public int getTotalSeatNumber() {
        return TotalSeatNumber;
    }

    public void setTotalSeatNumber(int TotalSeatNumber) {
        this.TotalSeatNumber = TotalSeatNumber;
    }

    public double getUsedAge() {
        return UsedAge;
    }

    public void setUsedAge(double UsedAge) {
        this.UsedAge = UsedAge;
    }
    @Override
    public String toString(){
        return this.ProductModel;
    }
    
    
}
