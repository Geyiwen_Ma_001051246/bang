/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author mage
 */
public class Customer {

    private String Name;
    private String Sex;
    private String AccountNumber;
    private String password;
    private ArrayList<Ticket> ticketlist;

    public Customer() {
        this.ticketlist = new ArrayList<Ticket>();
    }

    public ArrayList<Ticket> getTicket() {
        return ticketlist;
    }

    public void setTicket(ArrayList<Ticket> ticket) {
        this.ticketlist = ticket;
    }

    public void removeTicekt(Ticket ticket) {
        this.ticketlist.remove(ticket);
    }

    public Ticket addTicket() {
        Ticket ticket = new Ticket();
        ticketlist.add(ticket);
        return ticket;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(String AccountNumber) {
        this.AccountNumber = AccountNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String Sex) {
        this.Sex = Sex;
    }

}
