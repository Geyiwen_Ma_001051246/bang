/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author mage
 */
public class CustomerList {

    private ArrayList<Customer> customerlist;

    public CustomerList() {
        this.customerlist = new ArrayList<Customer>();
    }

    public ArrayList<Customer> getCustomerlist() {
        return customerlist;
    }

    public Customer addCustomer() {
        Customer c = new Customer();
        customerlist.add(c);

        return c;
    }

    public void setCustomer(Customer c) {
        customerlist.add(c);
    }
     
     public boolean checkAccountNumber(String accountNumber){
        for(Customer cust:customerlist){
            String custNumber=cust.getAccountNumber();
            if(custNumber.equals(accountNumber))
            {
                return false;
            }   
        }
        return true;
    }
     
    
    
    
    
    
    
}
