/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;


/**
 *
 * @author mage
 */
public class SeatList {

    private ArrayList<Seat> seatlist;

    private int totalrow;

    private int totalseatofarow;

    public SeatList() {
        this.seatlist = new ArrayList<Seat>();

    }

    public ArrayList<Seat> getSeatlist() {
        return seatlist;
    }

    public void setSeatlist(ArrayList<Seat> seatlist) {
        this.seatlist = seatlist;
    }

    public Seat addSeat() {
        Seat a = new Seat();
        seatlist.add(a);
        return a;
    }

    public void removeSeat(Seat a) {
        seatlist.remove(a);
    }

    public int getTotalrow() {
        return totalrow;
    }

    public void setTotalrow(int totalrow) {
        this.totalrow = totalrow;
    }

    public int getTotalseatofarow() {
        return totalseatofarow;
    }

    public void setTotalseatofarow(int totalseatofarow) {
        this.totalseatofarow = totalseatofarow;
    }

}
