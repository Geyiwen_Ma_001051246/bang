/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;


/**
 *
 * @author mage
 */
public class Seat {

    private int row;
    private int seateachrow;
    private Customer customer;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void removeCustomer() {
        customer = null;
    }

    public int getRow() {
        return row;

    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getSeateachrow() {
        return seateachrow;
    }

    public void setSeateachrow(int seateachrow) {
        this.seateachrow = seateachrow;
    }

    public String getSeatPosition() {
        String i = new String();
        switch (seateachrow) {
            case 1:
                i = "A";
                break;
            case 2:
                i = "B";
                break;
            case 3:
                i = "C";
                break;
            case 4:
                i = "D";
                break;
            case 5:
                i = "F";
                break;
            case 6:
                i = "H";
                break;
            case 7:
                i = "I";
                break;
            case 8:
                i = "K";
                break;

        }
        return row + i;
    }

    @Override
    public String toString() {
        String i = new String();
        switch (seateachrow) {
            case 1:
                i = "A";
                break;
            case 2:
                i = "B";
                break;
            case 3:
                i = "C";
                break;
            case 4:
                i = "D";
                break;
            case 5:
                i = "F";
                break;
            case 6:
                i = "H";
                break;
            case 7:
                i = "I";
                break;
            case 8:
                i = "K";
                break;

        }
        return row + i;
    }

}
