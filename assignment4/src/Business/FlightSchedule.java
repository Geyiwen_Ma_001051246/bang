/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author wangbeibei
 */
public class FlightSchedule {

    private ArrayList<Flight> flightlist;

    public FlightSchedule() {
        this.flightlist = new ArrayList<Flight>();
    }

    public Flight addFlight() {
        Flight flight = new Flight();
        flightlist.add(flight);
        return flight;
    }

    public void setFlight(Flight flight) {
        flightlist.add(flight);
    }

    public void removeFlight(Flight flight) {
        flightlist.remove(flight);

    }

    public ArrayList<Flight> getFlightlist() {
        return flightlist;
    }

    public void setFlightlist(ArrayList<Flight> flightlist) {
        this.flightlist = flightlist;
    }

    public FlightSchedule searchFlightbyfn(String FS) {
        FlightSchedule fll = new FlightSchedule();
        for (Flight flight : flightlist) {
            if (flight.getFlightnumber().equals(FS)) {
                fll.setFlight(flight);
            }
        }
        return fll;
    }

    public FlightSchedule searchFlightbyDL(String DL) {
        FlightSchedule fll = new FlightSchedule();
        for (Flight flight : flightlist) {
            if (flight.getDepartureLocation().equals(DL)) {
                fll.setFlight(flight);
            }
        }
        return fll;
    }

    public FlightSchedule searchFlightbyDT(String DT) {
        FlightSchedule fll = new FlightSchedule();
        for (Flight flight : flightlist) {
            if (flight.getDepartureTimeGeneral().equals(DT)) {
                fll.setFlight(flight);
            }
        }
        return fll;
    }

    public FlightSchedule searchFlightbyAL(String AL) {
        FlightSchedule fll = new FlightSchedule();
        for (Flight flight : flightlist) {
            if (flight.getArriveLocation().equals(AL)) {
                fll.setFlight(flight);
            }
        }
        return fll;
    }

    public FlightSchedule searchFlightbyAT(String AT) {
        FlightSchedule fll = new FlightSchedule();
        for (Flight flight : flightlist) {
            if (flight.getArriveTimeGeneral().equals(AT)) {
                fll.setFlight(flight);
            }
        }
        return fll;
    }
    
    public boolean checkFlight(String flightNum){
        for(Flight flight:flightlist){
            if(flight.getFlightnumber().equals(flightNum))
            {
                return false;
            }
        }
        return true;
    }
}
