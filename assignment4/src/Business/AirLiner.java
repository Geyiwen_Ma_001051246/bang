/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author mage
 */
public class AirLiner {
<<<<<<< Updated upstream
    /*This part is used to create and manage airliners.*/
=======

>>>>>>> Stashed changes
    private AirplaneDirectory airplanedirectory;
    private FlightSchedule flightschedule;
    private String Name;
    private String Abbreviate;
    private int flightnumber;
    private int airplanenumber;

    public AirLiner() {
        this.airplanedirectory = new AirplaneDirectory();
        this.flightschedule = new FlightSchedule();
    }

    public AirplaneDirectory getAirplanedirectory() {
        return airplanedirectory;
    }

    public void setAirplanedirectory(AirplaneDirectory airplanedirectory) {
        this.airplanedirectory = airplanedirectory;
    }

    public FlightSchedule getFlightschedule() {
        return flightschedule;
    }

    public void setFlightschedule(FlightSchedule flightschedule) {
        this.flightschedule = flightschedule;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getAbbreviate() {
        return Abbreviate;
    }

    public void setAbbreviate(String Abbreviate) {
        this.Abbreviate = Abbreviate;
    }

    public int getFlightnumber() {
        int flightnumber = 0;
        flightnumber = flightschedule.getFlightlist().size();
        return flightnumber;
    }

    public int getAirplanenumber() {
        int airplanenumber = 0;
        airplanenumber = airplanedirectory.getAirplanelist().size();
        return airplanenumber;
    }

    @Override
    public String toString() {
        return this.Name;
    }

}
