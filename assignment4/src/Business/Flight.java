/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author wangbeibei
 */
public class Flight {

    private String flightnumber;
    private GregorianCalendar DepartureTime;
    private String DepartureTimeGeneral;
    private String DepartureLocation;
    private GregorianCalendar ArriveTime;
    private String ArriveTimeGeneral;
    private String ArriveLocation;
    private Airplane airplane;
    private int SeatAvailable;
    private CustomerList customerlist;
    private SeatList seatmap;

    public Flight() {
        this.airplane = new Airplane();
        this.ArriveTime = new GregorianCalendar();
        this.DepartureTime = new GregorianCalendar();
        this.customerlist = new CustomerList();
        this.seatmap = new SeatList();
    }

    public SeatList getSeatmap() {
        seatmap.setTotalrow(airplane.getRow());
        seatmap.setTotalseatofarow(airplane.getSeatEachRow());
        return seatmap;
    }

    public void setSeatmap(SeatList seatmap) {
        this.seatmap = seatmap;
    }

    public CustomerList getCustomerlist() {
        return customerlist;
    }

    public void setCustomerlist(CustomerList customerlist) {
        this.customerlist = customerlist;
    }

    public GregorianCalendar getDepartureTime() {
        return DepartureTime;
    }

    public void setDepartureTime(GregorianCalendar DepartureTime) {
        this.DepartureTime = DepartureTime;
    }

    public GregorianCalendar getArriveTime() {
        return ArriveTime;
    }

    public void setArriveTime(GregorianCalendar ArriveTime) {
        this.ArriveTime = ArriveTime;
    }

    public Airplane getAirplane() {
        return airplane;
    }

    public void setAirplane(Airplane airplane) {
        this.airplane = airplane;
    }

    public String getFlightnumber() {
        return flightnumber;
    }

    public void setFlightnumber(String flightnumber) {
        this.flightnumber = flightnumber;
    }

    public String getDepartureTimeGeneral() {
        int i = DepartureTime.get(Calendar.HOUR);
        if (i < 6 && i >= 20) {
            this.DepartureTimeGeneral = "night";
        } else if (i >= 6 && i < 12) {
            this.DepartureTimeGeneral = "morning";
        } else {
            this.DepartureTimeGeneral = "day";
        }
        return DepartureTimeGeneral;
    }

    public String getDepartureLocation() {
        return DepartureLocation;
    }

    public void setDepartureLocation(String DepartureLocation) {
        this.DepartureLocation = DepartureLocation;
    }

    public String getArriveTimeGeneral() {
        int i = ArriveTime.get(Calendar.HOUR);
        if (i < 6 && i >= 20) {
            this.ArriveTimeGeneral = "night";
        } else if (i >= 6 && i < 12) {
            this.ArriveTimeGeneral = "morning";
        } else {
            this.ArriveTimeGeneral = "day";
        }
        return ArriveTimeGeneral;
    }

    public String getArriveLocation() {
        return ArriveLocation;
    }

    public void setArriveLocation(String ArriveLocation) {
        this.ArriveLocation = ArriveLocation;
    }

    public int getSeatAvailable() {
        int seat;
        seat = this.airplane.getTotalSeatNumber() - this.customerlist.getCustomerlist().size();
        return seat;
    }

    @Override
    public String toString() {
        return this.flightnumber;
    }

}
