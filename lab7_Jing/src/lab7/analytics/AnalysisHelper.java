/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import lab7.entities.Comment;
import lab7.entities.Post;
import lab7.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    // find user with Most Likes
    // TODO
    public void userWithMostLikes(){
        Map<Integer,User>userHashMap = DataStore.getInstance().getUsers();
        
        Map<Integer,Integer>tempUserHashMap = new HashMap<Integer,Integer>();
        
        for(User u : userHashMap.values()){
            for(Comment c: u.getComments()){
            int likes = 0;
            if(tempUserHashMap.containsKey(u.getId())){
                likes = tempUserHashMap.get(u.getId());
            }
            likes += c.getLikes();
            tempUserHashMap.put(u.getId(), likes);
        }
        }
        int max = 0;
        int maxId = 0;
        for(int id:tempUserHashMap.keySet()){
            if(tempUserHashMap.get(id)>max){
                max = tempUserHashMap.get(id);
                maxId = id;
            }
        }
            
       
        System.out.println("User With Most Likes:\t"+max+"\t Entire User Object\t"+userHashMap.get(maxId));
    }
    
    // find 5 comments which have the most likes
    // TODO
    public void topFiveComments(){
        Map<Integer,Comment>commentHashMap = DataStore.getInstance().getComments();
        List<Comment>commentList = new ArrayList<Comment>(commentHashMap.values());
        Collections.sort(commentList,new Comparator<Comment>(){
            @Override 
            public int compare(Comment o1,Comment o2){
                return o2.getLikes() - o1.getLikes();
            }
        });
            System.out.println("Printing the top five comments");
            
            for(int j=0;j<commentList.size()&&j<5;j++){
            System.out.println(commentList.get(j));
        }
        System.out.println("\n");
    }
    
    
    //Find Average number of likes per comment.
    public void getAverageLikesPerCommentsComments(){
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<Comment>(comments.values());
        int totalLikes = 0;
        int count = 0;
        for(Comment c: commentList){
            totalLikes += c.getLikes();
            count++;
        }
        
        System.out.println("The average number of likes per comment is:" + (totalLikes/count) +"\n");
    }
    
    
    //Find the post with most liked comments.
    public void getPostWithMostLikedComment(){
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        List<Post> postList = new ArrayList<Post>(posts.values());
        Map<Post, Integer>userPostCount = new HashMap<>();
        
        for(Post p: postList){
            int likes=0;
            for(Comment c: p.getComments()){
                //int likes = 0;
                
                likes += c.getLikes();
                //userPostCount.put(p, likes);
            }
            userPostCount.put(p, likes);
        }
        
        int max = 0;
        int maxId = 0;
        for(Post p : userPostCount.keySet()){
            if(userPostCount.get(p)> max){
                max = userPostCount.get(p);
                maxId = p.getPostId();
            }
        }
        
        System.out.println("The Post ID with most liked comments is :"+ maxId +"\n"+"The total likes is :" + max +"\n");
        
    }
    
    
    //Find the post with most comments.
    public void getPostWithMostComments(){
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        List<Post> postList = new ArrayList<Post>(posts.values());
//        for(Post p :postList){
//            System.out.println("+" +p.getPostId());
//        }
        Comparator<Post> test = new Comparator<Post>(){
            public int compare(Post p1, Post p2){
                return  p2.getComments().size() - p1.getComments().size();
            }
        };
       
        Collections.sort(postList, test);
        
        System.out.println("The ID of post with most comment is:"+ postList.get(0).getPostId()+ ". \nThe ID of user who posted the post is : "+ postList.get(0).getUserId());
        System.out.println();
    }
    
    
    //Top 5 inactive users based on total posts number.
    public void getInactiveUsersByPosts(){
        Map<Integer, Post> posts=DataStore.getInstance().getPosts();
        Map<Integer, User> users=DataStore.getInstance().getUsers();
        List<Post> postList=new ArrayList<Post>(posts.values());
        List<User> userList=new ArrayList<User>(users.values());
        //HashMap<List<User>,List<Integer>> hashMap = new HashMap<List<User>,List<Integer>>();    
        
        for(User u: users.values()){
            int postCount=0;
            for(Post p:postList){
                if(p.getUserId()==u.getId())
                {
                    postCount=postCount+1;
                }
            }
            u.setPosts(postCount);
        }
        
        Collections.sort(userList,new Comparator<User>(){
            @Override 
            public int compare(User u1,User u2){
                return u1.getPosts()-u2.getPosts();
            }
        });
        
            System.out.println("Printing the top 5 inactive users based on total posts number.");
            
            //int total=userList.size();
            for(int j=0;j<userList.size()&&j<5;j++){
            System.out.println(userList.get(j));
            System.out.println("Total posts :"+userList.get(j).getPosts());
        }
            System.out.println("\n");
    }
    
    
    //Top 5 inactive users based on total comments they created.
    public void getInactiveUsersByComments(){
        Map<Integer, Comment> comments=DataStore.getInstance().getComments();
        Map<Integer, User> users=DataStore.getInstance().getUsers();
        List<Comment> commentList=new ArrayList<Comment>(comments.values());
        List<User> userList=new ArrayList<User>(users.values());
        //HashMap<List<User>,List<Integer>> hashMap = new HashMap<List<User>,List<Integer>>();    
        
        for(User u: users.values()){
            int commentCount=0;
            for(Comment c:commentList){
                if(c.getUserId()==u.getId())
                {
                    commentCount=commentCount+1;
                }
            }
            u.setCommentsCount(commentCount);
        }
        
        Collections.sort(userList,new Comparator<User>(){
            @Override 
            public int compare(User u1,User u2){
                return u1.getCommentsCount()-u2.getCommentsCount();
            }
        });
        
            System.out.println("Printing top 5 inactive users based on total comments.");
            
            //int total=userList.size();
            for(int j=0;j<userList.size()&&j<5;j++){
            System.out.println(userList.get(j));
            System.out.println("Total comments :"+userList.get(j).getCommentsCount());
        }
            System.out.println("\n");
    }
    
    
    //Top 5 inactive users overall (sum of comments, posts and likes).
    public void getInactiveUsersByAll(){
        Map<Integer, Comment> comments=DataStore.getInstance().getComments();
        Map<Integer, User> users=DataStore.getInstance().getUsers();
        Map<Integer, Post> posts=DataStore.getInstance().getPosts();
        List<Comment> commentList=new ArrayList<Comment>(comments.values());
        List<User> userList=new ArrayList<User>(users.values());
        List<Post> postList=new ArrayList<Post>(posts.values());
        
        //HashMap<List<User>,List<Integer>> hashMap = new HashMap<List<User>,List<Integer>>();    
        
        for(User u: users.values()){
            int commentCount=0;
            for(Comment c:commentList){
                if(c.getUserId()==u.getId())
                {
                    commentCount=commentCount+1;
                }
            }
            u.setCommentsCount(commentCount);
        }
        
        for(User u: users.values()){
            int postCount=0;
            for(Post p:postList){
                if(p.getUserId()==u.getId())
                {
                    postCount=postCount+1;
                }
            }
            u.setPosts(postCount);
        }
        
        for(User u:users.values()){
            int likesCount=0;
            for(Comment c: commentList){
                if(c.getUserId()==u.getId()){
                    likesCount=likesCount+c.getLikes();
                }
            }
            u.setLikes(likesCount);
        }
        
        for(User u:users.values()){
            int totalCount;
            totalCount=u.getPosts()+u.getCommentsCount()+u.getLikes();
            u.setTotalCount(totalCount);
        }
        
        Collections.sort(userList,new Comparator<User>(){
            @Override 
            public int compare(User u1,User u2){
                return u1.getTotalCount()-u2.getTotalCount();
            }
        });
        
            System.out.println("Printing top 5 inactive users overall.");
            
            //int total=userList.size();
            for(int j=0;j<userList.size()&&j<5;j++){
            System.out.println(userList.get(j));
            System.out.println("number of posts :"+userList.get(j).getPosts()+"; number of comments: "+userList.get(j).getCommentsCount()+"; number of likes :"+userList.get(j).getLikes()+";\nTotal:"+userList.get(j).getTotalCount());
        }
            System.out.println("\n");
    }
    
    //Top 5 proactive users overall (sum of comments, posts and likes).
    public void getProactiveUsersByAll(){
        Map<Integer, Comment> comments=DataStore.getInstance().getComments();
        Map<Integer, User> users=DataStore.getInstance().getUsers();
        Map<Integer, Post> posts=DataStore.getInstance().getPosts();
        List<Comment> commentList=new ArrayList<Comment>(comments.values());
        List<User> userList=new ArrayList<User>(users.values());
        List<Post> postList=new ArrayList<Post>(posts.values());
        
        //HashMap<List<User>,List<Integer>> hashMap = new HashMap<List<User>,List<Integer>>();    
        
        for(User u: users.values()){
            int commentCount=0;
            for(Comment c:commentList){
                if(c.getUserId()==u.getId())
                {
                    commentCount=commentCount+1;
                }
            }
            u.setCommentsCount(commentCount);
        }
        
        for(User u: users.values()){
            int postCount=0;
            for(Post p:postList){
                if(p.getUserId()==u.getId())
                {
                    postCount=postCount+1;
                }
            }
            u.setPosts(postCount);
        }
        
        for(User u:users.values()){
            int likesCount=0;
            for(Comment c: commentList){
                if(c.getUserId()==u.getId()){
                    likesCount=likesCount+c.getLikes();
                }
            }
            u.setLikes(likesCount);
        }
        
        for(User u:users.values()){
            int totalCount;
            totalCount=u.getPosts()+u.getCommentsCount()+u.getLikes();
            u.setTotalCount(totalCount);
        }
        
        Collections.sort(userList,new Comparator<User>(){
            @Override 
            public int compare(User u1,User u2){
                return u2.getTotalCount()-u1.getTotalCount();
            }
        });
        
            System.out.println("Printing top 5 proactive users overall.");
            
            //int total=userList.size();
            for(int j=0;j<userList.size()&&j<5;j++){
            System.out.println(userList.get(j));
            System.out.println("number of posts :"+userList.get(j).getPosts()+"; number of comments: "+userList.get(j).getCommentsCount()+"; number of likes :"+userList.get(j).getLikes()+";\nTotal:"+userList.get(j).getTotalCount());
        }
            //System.out.println("\n");
    }
}
