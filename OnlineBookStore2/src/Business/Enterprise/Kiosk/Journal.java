package Business.Enterprise.Kiosk;

import Business.Enterprise.Item;

public class Journal extends Item {
	
	private Kiosk kiosk;
	
	public Journal(Kiosk kiosk,String name, double price) {
		super(name, price);
		// TODO Auto-generated constructor stub
		this.kiosk = kiosk;
	}

	public Kiosk getKiosk() {
		return this.kiosk;
	}
	
}