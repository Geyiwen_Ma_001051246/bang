package Business.Enterprise.BookStore;

import Business.Enterprise.Item;

public class Book extends Item {
	
	private Bookstore bookstore;
	
	public Book(Bookstore bookStore,String name, double price) {
		super(name, price);
		// TODO Auto-generated constructor stub
		this.bookstore = bookStore;
	}

	public Bookstore getBookstore() {
		return this.bookstore;
	}
	
}
