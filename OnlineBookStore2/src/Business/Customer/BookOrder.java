package Business.Customer;

import Business.Enterprise.Item;
import Business.Enterprise.ShopModel;
import Business.Enterprise.BookStore.Bookstore;
import Business.Enterprise.Store.Store;

public class BookOrder extends ItemOrder {

	private Bookstore bookstore;

    public BookOrder(ShopModel shopmodel, Item item, int quantity) {
        super(shopmodel, item, quantity);
        this.bookstore = (Bookstore) shopmodel;
    }

    public Bookstore getBookstore() {
        return this.bookstore;
    }

    @Override
    public ShopModel getShopModel() {
        return this.bookstore;
    }

}
