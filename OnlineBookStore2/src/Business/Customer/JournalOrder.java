package Business.Customer;

import Business.Enterprise.Item;
import Business.Enterprise.ShopModel;
import Business.Enterprise.Kiosk.Kiosk;

public class JournalOrder extends ItemOrder {

	private Kiosk kiosk;

    public JournalOrder(ShopModel shopmodel, Item item, int quantity) {
        super(shopmodel, item, quantity);
        this.kiosk = (Kiosk) shopmodel;
    }

    public Kiosk getKiosk() {
        return this.kiosk;
    }

    @Override
    public ShopModel getShopModel() {
        return this.kiosk;
    }

}
