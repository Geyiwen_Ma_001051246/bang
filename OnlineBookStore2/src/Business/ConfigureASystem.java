package Business;

import Business.Customer.Customer;
import Business.Employee.Employee;
import Business.Enterprise.BookStore.Book;
import Business.Enterprise.BookStore.Bookstore;
import Business.Enterprise.DeliveryCompany.DeliveryCompany;
import Business.Enterprise.Kiosk.Journal;
import Business.Enterprise.Kiosk.Kiosk;
import Business.Network.Network;
import Business.Organization.ManagerOrganization;
import Business.Organization.Organization;
import Business.Enterprise.Store.Product;
import Business.Enterprise.Store.Store;
import Business.Organization.DeliveryManOrganization;
import Business.Role.BossRole;
import Business.Role.DeliveryManRole;
import Business.Role.ManagerRole;
import Business.Role.SystemManagerRole;
import Business.UserAccount.UserAccount;

/**
 *
 * @author betterjing
 */
public class ConfigureASystem {

    public static EcoSystem configure() {

        EcoSystem system = EcoSystem.getInstance();

        //Create a network
        //create an enterprise
        //initialize some organizations
        //have some employees 
        //create user account
        // System manager, belongs to SYSTEM
        Employee employee1 = system.getEmployeeDirectory().createEmployee("Jing", "jingzou", "212212", "127998@qq.com");
        UserAccount ua1 = system.getUserAccountDirectory().createEmployeeAccount("sysadmin", "sysadmin", new SystemManagerRole(), employee1);

        // Customer, belongs to SYSTEM
        Customer c1 = system.getCustomers().createCustomer("2", "2", "545656@qq.com", "1231231234");
        UserAccount ua2 = system.getUserAccountDirectory().createCustomerAccount("2", "2", c1);

        Customer c2 = system.getCustomers().createCustomer("1", "1", "6556766@qq.com", "1231231234");
        UserAccount ua3 = system.getUserAccountDirectory().createCustomerAccount("1", "1", c2);
        
        Customer c3 = system.getCustomers().createCustomer("3", "3", "667776@qq.com", "1231231234");
        UserAccount ua10 = system.getUserAccountDirectory().createCustomerAccount("3", "3", c3);

        // MA NETWORK 
        Network network1 = system.createNetwork("Beijing");
        network1.setId("Beijing");
        Network network2 = system.createNetwork("Shanghai");
        network2.setId("Shanghai");

        // BOSTON Enterprise with organiztions created
        DeliveryCompany enter1 = network1.createDeliveryCompany("SF Delivery Company", "1 Pleasant Street, Beijing, MA 02125", "(617) 553-5900");
        enter1.setDescription("This is a delivery company.");
        enter1.setId("Delivery");
        enter1.setPath("Images/DeliveryCompanyCut/default.png");
        Employee boss = enter1.getEmployeeDirectory().createEmployee("boss", "boss", "23323", "boss@qq.com");
        UserAccount bossA = enter1.getUserAccountDirectory().createEmployeeAccount("delivery", "delivery", new BossRole(), boss);
        // BOSTON Delivery Company Organization
        ManagerOrganization mo1 = (ManagerOrganization) enter1.getOrganizationDirectory().getTypicalOrganization(Organization.Type.Manager);
        DeliveryManOrganization do1 = (DeliveryManOrganization) enter1.getOrganizationDirectory().getTypicalOrganization(Organization.Type.DeliveryMan);
        Employee employee2 = mo1.getEmployeeDirectory().createEmployee("Manager", "Manager", "111", "manager@qq.com");
        UserAccount ua4 = mo1.getUserAccountDirectory().createEmployeeAccount("m", "m", new ManagerRole(), employee2);

        Employee employee3 = mo1.getEmployeeDirectory().createEmployee("Delivery", "Man", "1111", "deliveryman@qq.com");
        UserAccount ua5 = mo1.getUserAccountDirectory().createEmployeeAccount("d", "d", new DeliveryManRole(), employee3);

        Employee employee4 = do1.getEmployeeDirectory().createEmployee("Delivery", "Man", "1111", "deliveryman1@qq.com");
        UserAccount ua8 = do1.getUserAccountDirectory().createEmployeeAccount("dd", "dd", new DeliveryManRole(), employee4);
        
        DeliveryCompany enter2 = network2.createDeliveryCompany("Shanghai SF Delivery Company", "1 Pleasant Street, Beijing, MA 02125", "(617) 553-5900");
        enter2.setDescription("This is a delivery company.");
        enter2.setId("Shanghai Delivery");
        enter2.setPath("Images/DeliveryCompanyCut/default.png");
        Employee boss1 = enter2.getEmployeeDirectory().createEmployee("boss Dc", "boss Dc", "23323", "boss@qq.com");
        UserAccount boss2 = enter2.getUserAccountDirectory().createEmployeeAccount("deliveryBoss", "deliveryBoss", new BossRole(), boss);
        // BOSTON Delivery Company Organization
        ManagerOrganization mo9 = (ManagerOrganization) enter2.getOrganizationDirectory().getTypicalOrganization(Organization.Type.Manager);
        DeliveryManOrganization do9 = (DeliveryManOrganization) enter2.getOrganizationDirectory().getTypicalOrganization(Organization.Type.DeliveryMan);
        Employee employee9 = mo9.getEmployeeDirectory().createEmployee("Manager9", "Manager9", "111", "manager@qq.com");
        UserAccount ua9 = mo9.getUserAccountDirectory().createEmployeeAccount("m9", "m9", new ManagerRole(), employee9);

        Employee employee10 = mo9.getEmployeeDirectory().createEmployee("Delivery10", "Man", "1111", "deliveryman@qq.com");
        UserAccount ua11 = mo9.getUserAccountDirectory().createEmployeeAccount("d10", "d10", new DeliveryManRole(), employee10);


        
//        Employee e = res2.getEmployeeDirectory().createEmployee("Legal", "Boss", "222", "boss@demo.com");
//        UserAccount ee = res2.getUserAccountDirectory().createEmployeeAccount("legal", "legal", new BossRole(), e);
//        
//        ManagerOrganization mo3 = (ManagerOrganization) res2.getOrganizationDirectory().getTypicalOrganization(Organization.Type.Manager);
//        Employee em2 = mo3.getEmployeeDirectory().createEmployee("Manager", "Manager", "222", "manager@demo.com");
//        UserAccount ua7 = mo3.getUserAccountDirectory().createEmployeeAccount("lm", "lm", new ManagerRole(), em2);

        
        
        //bookstore
        Bookstore bookstore = network1.createBookstore("Xinhua bookStore", "15 Westland Ave, Beijing, MA 02115", "(617) 375-1010");
        bookstore.setId("Xinhua bookStore");
        bookstore.setPath("Images/StoreCut/default.png");
        bookstore.setCategory(Bookstore.BookstoreCategory.Chinese);
        bookstore.setDescription("There are many books.");
        Book book1 = new Book(bookstore, "Wuthering Heights", 55);
        Book book2 = new Book(bookstore, "Jane Eyre", 66);
        Book book3 = new Book(bookstore, "Middlemarch", 49);
        Book book4 = new Book(bookstore, "Frankenstein", 75);
        Book book5 = new Book(bookstore, "Mrs Dalloway", 45);
        bookstore.addBookToList(book1);
        bookstore.addBookToList(book2);
        bookstore.addBookToList(book3);
        bookstore.addBookToList(book4);
        bookstore.addBookToList(book5);
        
        Employee be1 = bookstore.getEmployeeDirectory().createEmployee("bookboss", "bookboss", "222", "boss@qq.com");
        UserAccount bu1 = bookstore.getUserAccountDirectory().createEmployeeAccount("bookboss", "bookboss", new BossRole(), be1);
        
        ManagerOrganization bm1 = (ManagerOrganization) bookstore.getOrganizationDirectory().getTypicalOrganization(Organization.Type.Manager);
        Employee be2 = bm1.getEmployeeDirectory().createEmployee("Manager", "Manager", "222", "manager@qq.com");
        UserAccount bu2 = bm1.getUserAccountDirectory().createEmployeeAccount("bs", "bs", new ManagerRole(), be2);
        
        Bookstore bookstore1 = network1.createBookstore("Beijing Xinhua bookStore", "15 Westland Ave, Beijing, MA 02115", "(617) 375-1010");
        bookstore1.setId("Beijing Xinhua bookStore");
        bookstore1.setPath("Images/StoreCut/default.png");
        bookstore1.setCategory(Bookstore.BookstoreCategory.Chinese);
        bookstore1.setDescription("There are many books.");
        Book book6 = new Book(bookstore1, "Wuthering Heights1", 55);
        Book book7 = new Book(bookstore1, "Jane Eyre1", 66);
        Book book8 = new Book(bookstore1, "Middlemarch1", 49);
        Book book9 = new Book(bookstore1, "Frankenstein1", 75);
        Book book10 = new Book(bookstore1, "Mrs Dalloway1", 45);
        bookstore1.addBookToList(book6);
        bookstore1.addBookToList(book7);
        bookstore1.addBookToList(book8);
        bookstore1.addBookToList(book9);
        bookstore1.addBookToList(book10);
        
        Employee be3 = bookstore1.getEmployeeDirectory().createEmployee("bookboss1", "bookboss1", "222", "boss1@qq.com");
        UserAccount bu3 = bookstore1.getUserAccountDirectory().createEmployeeAccount("bookboss1", "bookboss1", new BossRole(), be3);
        
        ManagerOrganization bm2 = (ManagerOrganization) bookstore1.getOrganizationDirectory().getTypicalOrganization(Organization.Type.Manager);
        Employee be4 = bm2.getEmployeeDirectory().createEmployee("Manager1", "Manager1", "222", "manager@qq.com");
        UserAccount bu4 = bm2.getUserAccountDirectory().createEmployeeAccount("bs1", "bs1", new ManagerRole(), be4);
        
        
        Bookstore bookstore2 = network2.createBookstore("Shanghai Xinhua bookStore", "15 Westland Ave, Shanghai, MA 02115", "(617) 375-1010");
        bookstore2.setId("Shanghai Xinhua bookStore");
        bookstore2.setPath("Images/StoreCut/default.png");
        bookstore2.setCategory(Bookstore.BookstoreCategory.Chinese);
        bookstore2.setDescription("There are many books.");
        Book book11 = new Book(bookstore2, "Wuthering Heights2", 55);
        Book book12 = new Book(bookstore2, "Jane Eyre2", 66);
        Book book13 = new Book(bookstore2, "Middlemarch2", 49);
        Book book14 = new Book(bookstore2, "Frankenstein2", 75);
        Book book15 = new Book(bookstore2, "Mrs Dalloway2", 45);
        bookstore2.addBookToList(book11);
        bookstore2.addBookToList(book12);
        bookstore2.addBookToList(book13);
        bookstore2.addBookToList(book14);
        bookstore2.addBookToList(book15);
        
        Employee be5 = bookstore2.getEmployeeDirectory().createEmployee("bookboss5", "bookboss5", "222", "boss@qq.com");
        UserAccount bu5 = bookstore2.getUserAccountDirectory().createEmployeeAccount("bookboss5", "bookboss5", new BossRole(), be5);
        
        ManagerOrganization bm3 = (ManagerOrganization) bookstore2.getOrganizationDirectory().getTypicalOrganization(Organization.Type.Manager);
        Employee be6 = bm3.getEmployeeDirectory().createEmployee("Manager6", "Manager6", "222", "manager@qq.com");
        UserAccount bu6 = bm3.getUserAccountDirectory().createEmployeeAccount("bs6", "bs6", new ManagerRole(), be6);
        
        Bookstore bookstore3 = network2.createBookstore("Shanghai Xinhua bookStore 1", "15 Westland Ave, Shanghai, MA 02115", "(617) 375-1010");
        bookstore3.setId("Shanghai Xinhua bookStore 1");
        bookstore3.setPath("Images/StoreCut/default.png");
        bookstore3.setCategory(Bookstore.BookstoreCategory.Chinese);
        bookstore3.setDescription("There are many books.");
        Book book16 = new Book(bookstore3, "Wuthering Heights3", 55);
        Book book17 = new Book(bookstore3, "Jane Eyre3", 66);
        Book book18 = new Book(bookstore3, "Middlemarch3", 49);
        Book book19 = new Book(bookstore3, "Frankenstein3", 75);
        Book book20 = new Book(bookstore3, "Mrs Dalloway3", 45);
        bookstore3.addBookToList(book16);
        bookstore3.addBookToList(book17);
        bookstore3.addBookToList(book18);
        bookstore3.addBookToList(book19);
        bookstore3.addBookToList(book20);
        
        Employee be7 = bookstore3.getEmployeeDirectory().createEmployee("bookboss7", "bookboss7", "222", "boss1@qq.com");
        UserAccount bu7 = bookstore3.getUserAccountDirectory().createEmployeeAccount("bookboss7", "bookboss7", new BossRole(), be7);
        
        ManagerOrganization bm4 = (ManagerOrganization) bookstore3.getOrganizationDirectory().getTypicalOrganization(Organization.Type.Manager);
        Employee be8 = bm4.getEmployeeDirectory().createEmployee("Manager8", "Manager8", "222", "manager@qq.com");
        UserAccount bu8 = bm4.getUserAccountDirectory().createEmployeeAccount("bs8", "bs8", new ManagerRole(), be8);
        
        
      //Kiosk
        Kiosk kiosk = network1.createKiosk("School Kiosk", "15 Westland Ave, Beijing, MA 02115", "(617) 375-1010");
        kiosk.setId("School Kiosk");
        kiosk.setPath("Images/StoreCut/default.png");
        kiosk.setCategory(Kiosk.KioskCategory.Chinese);
        kiosk.setDescription("There are many magazine.");
        Journal journal1 = new Journal(kiosk, "Chemistry of Materials", 23);
        Journal journal2 = new Journal(kiosk, "Physical Review letters", 35);
        Journal journal3 = new Journal(kiosk, "the Journal of the American Chemical Society", 20);
        Journal journal4 = new Journal(kiosk, "Applied Physics Letters", 33);
        Journal journal5 = new Journal(kiosk, "Angewandte Chemie International Edition", 45);
        kiosk.addJournalToList(journal1);
        kiosk.addJournalToList(journal2);
        kiosk.addJournalToList(journal3);
        kiosk.addJournalToList(journal4);
        kiosk.addJournalToList(journal5);
        
        Employee ke1 = kiosk.getEmployeeDirectory().createEmployee("journalboss", "journalboss", "222", "journalboss@qq.com");
        UserAccount ku1 = kiosk.getUserAccountDirectory().createEmployeeAccount("journalboss", "journalboss", new BossRole(), ke1);
        
        ManagerOrganization km1 = (ManagerOrganization) kiosk.getOrganizationDirectory().getTypicalOrganization(Organization.Type.Manager);
        Employee ke2 = km1.getEmployeeDirectory().createEmployee("Manager", "Manager", "222", "manager@qq.com");
        UserAccount ku2 = km1.getUserAccountDirectory().createEmployeeAccount("ks", "ks", new ManagerRole(), ke2);
        
        
        Kiosk kiosk1 = network1.createKiosk("Beijing School Kiosk", "15 Westland Ave, Beijing, MA 02115", "(617) 375-1010");
        kiosk1.setId("Beijing School Kiosk");
        kiosk1.setPath("Images/StoreCut/default.png");
        kiosk1.setCategory(Kiosk.KioskCategory.Chinese);
        kiosk1.setDescription("There are many magazine.");
        Journal journal6 = new Journal(kiosk1, "Chemistry of Materials1", 23);
        Journal journal7 = new Journal(kiosk1, "Physical Review letters1", 35);
        Journal journal8 = new Journal(kiosk1, "the Journal of the American Chemical Society1", 20);
        Journal journal9 = new Journal(kiosk1, "Applied Physics Letters1", 33);
        Journal journal10 = new Journal(kiosk1, "Angewandte Chemie International Edition1", 45);
        kiosk1.addJournalToList(journal6);
        kiosk1.addJournalToList(journal7);
        kiosk1.addJournalToList(journal8);
        kiosk1.addJournalToList(journal9);
        kiosk1.addJournalToList(journal10);
        
        Employee ke3 = kiosk1.getEmployeeDirectory().createEmployee("journalboss3", "journalboss3", "222", "journalboss@qq.com");
        UserAccount ku3 = kiosk1.getUserAccountDirectory().createEmployeeAccount("journalboss3", "journalboss3", new BossRole(), ke3);
        
        ManagerOrganization km2 = (ManagerOrganization) kiosk1.getOrganizationDirectory().getTypicalOrganization(Organization.Type.Manager);
        Employee ke4 = km2.getEmployeeDirectory().createEmployee("Manager4", "Manager4", "222", "manager@qq.com");
        UserAccount ku4 = km2.getUserAccountDirectory().createEmployeeAccount("ks4", "ks4", new ManagerRole(), ke4);
        
        
        Kiosk kiosk3 = network2.createKiosk("Shanghai School Kiosk", "15 Westland Ave, Shanghai, MA 02115", "(617) 375-1010");
        kiosk3.setId("Shanghai School Kiosk");
        kiosk3.setPath("Images/StoreCut/default.png");
        kiosk3.setCategory(Kiosk.KioskCategory.Chinese);
        kiosk3.setDescription("There are many magazine.");
        Journal journal11 = new Journal(kiosk3, "Chemistry of Materials3", 23);
        Journal journal12 = new Journal(kiosk3, "Physical Review letters3", 35);
        Journal journal13 = new Journal(kiosk3, "the Journal of the American Chemical Society3", 20);
        Journal journal14 = new Journal(kiosk3, "Applied Physics Letters3", 33);
        Journal journal15 = new Journal(kiosk3, "Angewandte Chemie International Edition3", 45);
        kiosk3.addJournalToList(journal11);
        kiosk3.addJournalToList(journal12);
        kiosk3.addJournalToList(journal13);
        kiosk3.addJournalToList(journal14);
        kiosk3.addJournalToList(journal15);
        
        Employee ke5 = kiosk3.getEmployeeDirectory().createEmployee("journalboss5", "journalboss5", "222", "journalboss@qq.com");
        UserAccount ku5 = kiosk3.getUserAccountDirectory().createEmployeeAccount("journalboss5", "journalboss5", new BossRole(), ke5);
        
        ManagerOrganization km6 = (ManagerOrganization) kiosk3.getOrganizationDirectory().getTypicalOrganization(Organization.Type.Manager);
        Employee ke6 = km6.getEmployeeDirectory().createEmployee("Manager6", "Manager6", "222", "manager@qq.com");
        UserAccount ku6 = km6.getUserAccountDirectory().createEmployeeAccount("ks6", "ks6", new ManagerRole(), ke6);
        
        
        Kiosk kiosk7 = network2.createKiosk("Shanghai School Kiosk 1", "15 Westland Ave, Shanghai, MA 02115", "(617) 375-1010");
        kiosk7.setId("Shanghai School Kiosk 1");
        kiosk7.setPath("Images/StoreCut/default.png");
        kiosk7.setCategory(Kiosk.KioskCategory.Chinese);
        kiosk7.setDescription("There are many magazine.");
        Journal journal16 = new Journal(kiosk7, "Chemistry of Materials7", 23);
        Journal journal17 = new Journal(kiosk7, "Physical Review letters7", 35);
        Journal journal18 = new Journal(kiosk7, "the Journal of the American Chemical Society7", 20);
        Journal journal19 = new Journal(kiosk7, "Applied Physics Letters7", 33);
        Journal journal20 = new Journal(kiosk7, "Angewandte Chemie International Edition7", 45);
        kiosk7.addJournalToList(journal16);
        kiosk7.addJournalToList(journal17);
        kiosk7.addJournalToList(journal18);
        kiosk7.addJournalToList(journal19);
        kiosk7.addJournalToList(journal20);
        
        Employee ke7 = kiosk7.getEmployeeDirectory().createEmployee("journalboss7", "journalboss7", "222", "journalboss@qq.com");
        UserAccount ku7 = kiosk7.getUserAccountDirectory().createEmployeeAccount("journalboss7", "journalboss7", new BossRole(), ke7);
        
        ManagerOrganization km8 = (ManagerOrganization) kiosk7.getOrganizationDirectory().getTypicalOrganization(Organization.Type.Manager);
        Employee ke8 = km8.getEmployeeDirectory().createEmployee("Manager8", "Manager8", "222", "manager@qq.com");
        UserAccount ku8 = km8.getUserAccountDirectory().createEmployeeAccount("ks8", "ks8", new ManagerRole(), ke8);
        
        return system;
    }

}
