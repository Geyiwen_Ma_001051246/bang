/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.DeliveryCompany.DeliveryCompany;
import Business.Enterprise.Kiosk.Kiosk;
import Business.Enterprise.Enterprise;
import Business.Enterprise.BookStore.Bookstore;
import Business.Enterprise.Store.Store;
import Business.Network.Network;
import Business.UserAccount.UserAccount;
import UserInterface.BookStore.BookStoreManagerMainJPanel;
import UserInterface.DeliveryCompany.Manager.DeliveryCompanyManagerMainJPanel;
import UserInterface.Kiosk.KioskManagerMainJPanel;
import UserInterface.StoreManagerMainJPanel.StoreManagerMainJPanel;
import java.awt.CardLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author betterjing
 */
public class BossRole extends Role {

    public BossRole() {
        super(RoleType.Boss);
    }

    @Override
    public void createWorkArea(EcoSystem system, JPanel container, UserAccount userAccount, Network net, Enterprise en, JFrame frame) {
        if (en instanceof DeliveryCompany) {
            DeliveryCompanyManagerMainJPanel cp = new DeliveryCompanyManagerMainJPanel(system, container, net, en, userAccount, frame, this);
            container.add(cp);
            CardLayout layout = (CardLayout) container.getLayout();
            layout.next(container);
        }
        if (en instanceof Store) {
        	JOptionPane.showMessageDialog(null, "Username/Password doesn't match our records.");
        }
        if (en instanceof Bookstore) {
        	BookStoreManagerMainJPanel cp = new BookStoreManagerMainJPanel(system, container, net, en, userAccount, frame, this);
            container.add(cp);
            CardLayout layout = (CardLayout) container.getLayout();
            layout.next(container);
        }
        if (en instanceof Kiosk) {
        	KioskManagerMainJPanel cp = new KioskManagerMainJPanel(system, container, net, en, userAccount, frame, this);
            container.add(cp);
            CardLayout layout = (CardLayout) container.getLayout();
            layout.next(container);
        }
    }

    @Override
    public String toString() {
        return RoleType.Boss.getValue();
    }
}
