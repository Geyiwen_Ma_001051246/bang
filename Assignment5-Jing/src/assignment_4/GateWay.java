/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4;

import assignment_4.analytics.AnalysisHelper;
import assignment_4.analytics.DataStore;
import assignment_4.entities.Customer;
import assignment_4.entities.Item;
import assignment_4.entities.Order;
import assignment_4.entities.Product;
import assignment_4.entities.SalesPerson;
import java.io.IOException;
import java.util.Map;

/**
 *
 * @author harshalneelkamal
 */
public class GateWay {
    DataReader productReader;
    DataReader orderReader;
    AnalysisHelper helper;

    public GateWay() throws IOException {
        DataGenerator generator = DataGenerator.getInstance();
        orderReader = new DataReader(generator.getOrderFilePath());
        productReader = new DataReader(generator.getProductCataloguePath());
        helper = new AnalysisHelper();
    }


    public static void main(String args[]) throws IOException {
        GateWay gate = new GateWay();
        gate.readData();
    }

    private void readData() throws IOException {
        String[] row;
        while ((row = productReader.getNextRow()) != null) {
            generateProduct(row);
        }
        while ((row = orderReader.getNextRow()) != null) {
            Order o = generateOrder(row);
            generateCustomer(row, o);
            generateSales(row, o);
        }

        runAnalysis();
    }

    private void generateProduct(String[] row) {
        int id = Integer.parseInt(row[0]);
        int max = Integer.parseInt(row[2]);
        int min = Integer.parseInt(row[1]);
        int target = Integer.parseInt(row[3]);
        Product product = new Product(id, min, max, target);
        DataStore.getInstance().getProducts().put(id, product);
    }

    private Order generateOrder(String[] row) {
        int orderid = Integer.parseInt(row[0]);
        int itemid = Integer.parseInt(row[1]);
        int productid = Integer.parseInt(row[2]);
        int quantity = Integer.parseInt(row[3]);
        int salesid = Integer.parseInt(row[4]);
        int customerid = Integer.parseInt(row[5]);
        int salesprice = Integer.parseInt(row[6]);
        Item item = new Item(itemid, productid, salesprice, quantity);
        Order order = new Order(orderid, salesid, customerid, item);
        DataStore.getInstance().getItems().put(itemid, item);
        DataStore.getInstance().getOrders().put(orderid, order);
        return order;
    }

    private void generateCustomer(String[] row, Order o) {
        int customerid = Integer.parseInt(row[5]);
        Map<Integer, Customer> cutomers = DataStore.getInstance().getCustomers();
        if (cutomers.containsKey(customerid)) {
            cutomers.get(customerid).getOrders().add(o);
        } else {
            Customer c = new Customer(customerid);
            c.getOrders().add(o);
            cutomers.put(customerid, c);
        }
    }

    private void generateSales(String[] row, Order o) {
        int salesid = Integer.parseInt(row[4]);
        Map<Integer, SalesPerson> sales = DataStore.getInstance().getSales();
        if (sales.containsKey(salesid)) {
            sales.get(salesid).getOrders().add(o);
        } else {
            SalesPerson s = new SalesPerson(salesid);
            s.getOrders().add(o);
            sales.put(salesid, s);
        }
    }

    private void runAnalysis() {
        helper.ThreeMostPopularProduct();
        helper.ThreeBestCustomers();
        
    }


}
