/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4.analytics;

import assignment_4.entities.*;

import java.util.*;

/**
 * @author amon1895
 */
public class AnalysisHelper {
//target price
    public void ThreeMostPopularProduct() {
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        Map<Integer, Item> items = DataStore.getInstance().getItems();
        Map<Integer, Integer> productSoldCountMap = new HashMap<Integer, Integer>();
        for (Item item : items.values()) {
            int productId = item.getProductId();
            Product product = products.get(productId);
            if (item.getSalesPrice() > product.getTargetPrice()) {                
                int count = productSoldCountMap.getOrDefault(productId, 0);
                productSoldCountMap.put(productId, count + item.getQuantity());
            }
        }      
        
        List<Map.Entry<Integer, Integer>> productSoldCountRankMap = new ArrayList<Map.Entry<Integer, Integer>>(productSoldCountMap.entrySet());
        Collections.sort(productSoldCountRankMap, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return o2.getValue() - o1.getValue();
            }
        });

        System.out.println("\n3 Most Popular Products: ");
        int forcount=1;
        for (int i = 0; i < productSoldCountRankMap.size(); ) {
            System.out.println("Top"+forcount+":");
            System.out.println("Product id: " + productSoldCountRankMap.get(i).getKey() + " count: " + productSoldCountRankMap.get(i).getValue());
            for(int j=i+1;j<productSoldCountRankMap.size();j++){
            if(productSoldCountRankMap.get(j).getValue().equals(productSoldCountRankMap.get(i).getValue())){
            System.out.println("Product id: " + productSoldCountRankMap.get(j).getKey() + " count: " + productSoldCountRankMap.get(j).getValue());
            //j=j+1;
            }
            else{
                forcount=forcount+1;
                i=j;
                //System.out.println("\n");
                break;
            }
            }
           if(forcount>3){
               break;
           }
        }

    }
//target price
    public void ThreeBestCustomers() {
        Map<Integer, Customer> customers = DataStore.getInstance().getCustomers();
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        Map<Integer, Integer> customerPriceDetaMap= new HashMap<>();
        for (Customer customer : customers.values()) {
            int detaSum = 0;
            for (Order order : customer.getOrders()) {
                Product product = products.get(order.getItem().getProductId());
                // 销售价格(sale price)与目标价格(target price)之间的差
                int deta = Math.abs(order.getItem().getSalesPrice() - product.getTargetPrice());
                detaSum += deta * order.getItem().getQuantity();
            }
            customerPriceDetaMap.put(customer.getCustomerId(), Math.abs(detaSum));
        }

        List<Map.Entry<Integer, Integer>> customerPriceRankMap = new ArrayList<Map.Entry<Integer, Integer>>(customerPriceDetaMap.entrySet());
        Collections.sort(customerPriceRankMap, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return o1.getValue() - o2.getValue();
            }
        });

        System.out.println("\n3 Best Customer: ");
        int forcount=1;
        for (int i = 0; i < customerPriceRankMap.size(); ) {
            System.out.println("Top"+forcount+":");
            System.out.println("Customer id: " + customerPriceRankMap.get(i).getKey() + " deta: " + customerPriceRankMap.get(i).getValue());
            for(int j=i+1;j<customerPriceRankMap.size();j++){
            if(customerPriceRankMap.get(j).getValue().equals(customerPriceRankMap.get(i).getValue())){
            System.out.println("Customer id: " + customerPriceRankMap.get(j).getKey() + " deta: " + customerPriceRankMap.get(j).getValue());
            //j=j+1;
            }
            else{
                forcount=forcount+1;
                i=j;
                //System.out.println("\n");
                break;
            }
            }
           if(forcount>3){
               break;
           }
        }
    }

   
}
