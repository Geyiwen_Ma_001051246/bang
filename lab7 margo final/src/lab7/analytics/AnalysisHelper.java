/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7.analytics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lab7.entities.Comment;
import lab7.entities.Post;
import lab7.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    // find user with Most Likes
    // TODO
    public void userWithMostLikes(){
        Map<Integer,User>userHashMap = DataStore.getInstance().getUsers();
        
        Map<Integer,Integer>tempUserHashMap = new HashMap<Integer,Integer>();
        
        for(User u : userHashMap.values()){
            for(Comment c: u.getComments()){
            int likes = 0;
            if(tempUserHashMap.containsKey(u.getId())){
                likes = tempUserHashMap.get(u.getId());
            }
            likes += c.getLikes();
            tempUserHashMap.put(u.getId(), likes);
        }
        }
        int max = 0;
        int maxId = 0;
        for(int id:tempUserHashMap.keySet()){
            if(tempUserHashMap.get(id)>max){
                max = tempUserHashMap.get(id);
                maxId = id;
            }
        }
            
       
        System.out.println("User With Most Likes:\t"+max+"\t Entire User Object\t"+userHashMap.get(maxId));
    }
    
    // find 5 comments which have the most likes
    // TODO
    public void topFiveComments(){
        Map<Integer,Comment>commentHashMap = DataStore.getInstance().getComments();
        List<Comment>commentList = new ArrayList<>(commentHashMap.values());
        Collections.sort(commentList,new Comparator<Comment>(){
            @Override 
            public int compare(Comment o1,Comment o2){
                return o2.getLikes() - o1.getLikes();
            }
        });
            System.out.println("Printing the top five comments");
            
            for(int j=0;j<commentList.size()&&j<5;j++){
            System.out.println(commentList.get(j));
        }
    }
    
    public void getAverageLikesPerCommentsComments(){
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<Comment>(comments.values());
        int totalLikes = 0;
        int count = 0;
        for(Comment c: commentList){
            totalLikes += c.getLikes();
            count++;
        }
        
        System.out.println("The average number of likes per comment is:" + (totalLikes/count) +"\n");
    }
    
    
    public void getPostWithMostLikedComment(){
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        List<Post> postList = new ArrayList<Post>(posts.values());
        Map<Post, Integer>userPostCount = new HashMap<>();
        
        for(Post p: postList){
            for(Comment c: p.getComments()){
                int likes = 0;
                
                likes += c.getLikes();
                userPostCount.put(p, likes);
            }
        }       
        int max = 0;
        int maxId = 0;
        for(Post p : userPostCount.keySet()){
            if(userPostCount.get(p)> max){
                max = userPostCount.get(p);
                maxId = p.getPostId();
            }
        }
        
        System.out.println("The Post ID with most liked comments is :"+ maxId +"\n"+"The total likes is :" + max +"\n");
        
    }
    
    // find post with Most comments
    // TODO
    public void getPostWithMostComments() {
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        List<Post> postList = new ArrayList<Post>(posts.values());
        Comparator<Post> test = new Comparator<Post>() {
            public int compare(Post p1, Post p2) {
                return p2.getComments().size() - p1.getComments().size();
            }
        };

        Collections.sort(postList, test);

        System.out.println("The ID of post with most comment is:" + postList.get(0).getPostId() + ". The ID of user who posted the post is: " + postList.get(0).getUserId());
        System.out.println();
    }
    
    
    
    
    
}

